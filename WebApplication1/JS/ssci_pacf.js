﻿function callPACF() {
    /*
     * List of needed parameters:
     *  - idSeries
     *  - maxLag as the number of lagged values to sum for each value of PACF vector
     *  - difference (ρ) as the times to multiply each lagged value in the sum
     */
    // Global parameters
    var numLines = 1;
    var lineNames = [];
    var datablock = [];
    var lines;
    var cht;

    // Loaded parameters from page
    var idSeries = document.getElementById("seriesId").value;
    var maxLag = document.getElementById("maxLag").value;
    var difference = document.getElementById("difference").value;

    d3.csv("/clientiGAP.csv", function (d) {
        return {
            date: new Date(),
            value: +d.val
        };
    }, function (error, data) {
        lineNames = ["Lag", "Partial ACF"];

        var begin = idSeries * 35;
        var end = begin + 35
        // Obtain desidered series
        datablock = data.slice(begin, end);

        // Define charting variables
        var chart_width = 600;
        var chart_height = 400;
        var margin_top = 30;
        var margin_bottom = 35;
        var margin_left = 80;
        var margin_right = 30;
        var chart_title = "Auto-correlation of values of clientiGAP.csv";
        var xaxis_title = "Lag";
        var yaxis_title = "Partial ACF";
        var tt_width = 100;

        // Create SVG object
        d3.select("#textArea").remove("textArea")
        d3.select("svg").remove("svg")
        cht = d3.select("#replaceable")
            .append("svg")
            .attr("class", "c_chart")
            .attr("width", chart_width)
            .attr("height", chart_height);
        

        // Scales
        var x_scale = d3.scale.linear()
            .domain([0, maxLag-1])
            .range([margin_left, chart_width - margin_right]);
        var y_scale = d3.scale.linear()
            .domain([-1, 1])
            .range([chart_height - margin_bottom, margin_top]);

        // X Axis
        var xAxis = d3.svg.axis()
            .scale(x_scale)
            .orient("bottom");

        cht.append("g")
            .attr("class", "c_x_axis")
            .attr("transform", "translate(0," + (chart_height - margin_bottom) + ")")
            .call(xAxis);

        // Y Axis
        var yAxis = d3.svg.axis()
            .scale(y_scale)
            .orient("left");

        cht.append("g")
            .attr("class", "c_y_axis")
            .attr("transform", "translate(" + margin_left + ",0)")
            .call(yAxis);

        // Chart title
        cht.append("text")
            .attr("y", 20)
            .attr("x", ((chart_width - margin_left - margin_right) / 2 + margin_left))
            .attr("class", "c_title")
            .text(chart_title);

        // Add axes titles
        cht.append("text")
            .attr("y", chart_height - margin_bottom + 30)
            .attr("x", ((chart_width - margin_left - margin_right) / 2 + margin_left))
            .attr("class", "c_axis_title")
            .text(xaxis_title);

        cht.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", margin_left - 50)
            .attr("x", -((chart_height - margin_top - margin_bottom) / 2 + margin_top))
            .attr("class", "c_axis_title")
            .text(yaxis_title);

        // Lines - acf
        var points = ssci.objectToPoints(datablock, "date", "value");
        var dObj = ssci.ts.pacf()
            .data(datablock)
            .x(function (d) { return d.date; })
            .y(function (d) { return d.value; })
            .diff(difference)
            .maxlag(maxLag);
        
        dObj();

        lines = d3.svg.line()
            .x(function (d) { return x_scale(d[0]); })
            .y(function (d) { return y_scale(d[1]); });

        cht.append("path")
            .data(dObj.output())
            .attr("d", lines(dObj.output()))
            .attr("class", "c_line1");

        // Grid lines on ticks
        var ticks = y_scale.ticks();
        ticks.shift();

        cht.selectAll(".c_y_grid")
            .data(ticks)
            .enter().append("path")
            .attr("d", function (d, i) { return "M" + margin_left + "," + y_scale(d) + "L" + (chart_width - margin_right) + "," + y_scale(d); })
            .attr("class", "c_y_grid");
        
    }
    );
}