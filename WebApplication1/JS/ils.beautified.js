﻿function runILS() {
    if (document.getElementById("maxo").checked)
        runILSChallenge()
    else
        runILSNoChallenge()
}

function runILSChallenge() {
    var alpha = $("#alpha").val();
    var max_time = $("#maxSeconds").val();

    var zlbBest = Number.MAX_VALUE

    var endTime = Date.now() + (max_time * 1000)

    while (endTime > Date.now()) {
        perturbateCurrentSolution(alpha)
        var current_z = opt10(c);
        if (zlbBest > current_z)
            zlbBest = current_z
    }

    if (zlbBest != Number.MAX_VALUE)
        $("#textArea").val($("#textArea").val() + "\nSoluzione ammissibile! Costi ottimizzati con ILS (zUB ottimizzato) = " + zlbBest + " with " + i + " iterations.");
    else
        $("#textArea").val($("#textArea").val() + "\nSoluzione non ammissibile.");
}

function runILSNoChallenge() {
    var alpha = $("#alpha").val();
    var max_iter = $("#iterations").val();
    var max_time = $("#maxSeconds").val();

    zlbBest = Number.MAX_VALUE
    var i = 0
    var current_z = 0

    var endTime = Date.now() + (max_time * 1000)

    while (i < max_iter & endTime > Date.now()) {
        perturbateCurrentSolution(alpha)
        current_z = opt10(c);
        if (zlbBest > current_z)
            zlbBest = current_z
        i++
    }
    if (current_z != Number.MAX_VALUE) {
        $("#textArea").val($("#textArea").val() + "\nSoluzione ammissibile! Costi ottimizzati con ILS (zUB ottimizzato) = " + zlbBest);
        if (i < max_iter)
            $("#textArea").val($("#textArea").val() + " (time out) - iteration count: " + i)
        else
            $("#textArea").val($("#textArea").val() + " (max iterations)")
    }
    else $("#textArea").val($("#textArea").val() + "\nSoluzione non ammissibile.");
}

/*
 * @TODO DOCS
 */
function perturbateCurrentSolution(alpha) {
    var localC = c;
    var perturbated_costs = new Array(m)
    for (var i = 0; i < m; i++) {
        perturbated_costs[i] = new Array(n)
    }
    var coefficient = (1 - alpha / 2)
    for (var i = 0; i < m; i++) {
        for (var j = 0; j < n; j++) {
            perturbated_costs[i][j] = coefficient * localC[i][j] + Math.random() * alpha * localC[i][j];
        }
    }
    opt10(perturbated_costs)
}

function opt10(cost) {
    var is_improved;
    var z = 0;
    var cap_left = cap.slice();
    var localReq = req;

    for (var j = 0; j < n; j++) {
        var solj = sol[j]
        cap_left[solj] -= localReq[solj][j];
        z += cost[solj][j];
    }
    do {
        is_improved = false;
        for (var j = 0; j < n; j++) { // Iterating through customers
            var solj = sol[j]
            var cost_soljj = cost[solj][j]
            for (var i = 0; i < m; i++) { // Iterating though facilities
                var cost_ij = cost[i][j]
                var req_ij = localReq[i][j]
                if (cost_ij < cost_soljj && cap_left[i] >= req_ij) { // current customerToFacility's cost < another & facility has capacity
                    cap_left[solj] += localReq[solj][j];
                    cap_left[i] -= req_ij;
                    z -= cost_soljj;

                    sol[j] = i;
                    z += cost_ij;

                    is_improved = true;
                    break;
                }
            }
            if (is_improved) break;
        }
    } while (is_improved);
    return z;
}