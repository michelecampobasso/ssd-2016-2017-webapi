function readWithCORS() {
    var resp;
    var req = new XMLHttpRequest();
    inText = document.getElementById("inputRemote").value;

    $.get('http://astarte.csr.unibo.it/gapdata/homemade/' + inText, function (data) {
        jInstance = data;
        setInstance(jInstance);
    }, "json");

    if (document.getElementById("defaults").checked) {
        if (inText == "FAPGAP.json") {
            $("#alpha").val("1.9");
            $("#candListSize").val("50");
        }

        if (inText == "romagna.json") {
            $("#alpha").val("1.0");
            $("#candListSize").val("90");
        }
    }
}


// elaborazione dei dati letti in locale
function receivedText(e) {
    istanza = e.target.result;
    jInstance = JSON.parse(istanza);
    setInstance(jInstance);
}

function setInstance(jInstance) {
    n = jInstance.numcustomers;   // num clienti
    m = jInstance.numfacilities;  // num server
    c = jInstance.cost;   // matrice dei costi
    req = jInstance.req;  // matrice delle richieste
    cap = jInstance.cap;  // vattore delle capacità
    $("#textArea").val("Istanza " + jInstance.name + " caricata con n=" + n +".");
}

function checkSol(sol) {
    var z = 0, j;
    var capused = new Array(m);
    for (i = 0; i < m; i++) capused[i] = 0;
    // controllo assegnamenti
    for (j = 0; j < n; j++)
        if (sol[j] < 0 || sol[j] >= m || sol[j] === undefined) {
            z = Number.MAX_VALUE;
            return z;
        }
        else
            z += c[sol[j]][j];
    // controllo capacità
    for (j = 0; j < n; j++) {
        capused[sol[j]] += req[sol[j]][j];
        if (capused[sol[j]] > cap[sol[j]]) {
            z = Number.MAX_VALUE;
            return z;
        }
    }
    return z;
}