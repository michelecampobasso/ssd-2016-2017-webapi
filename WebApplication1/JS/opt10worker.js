﻿self.onmessage = function (msg) {
    let zz = 0
    for (var j = msg.data.start; j < msg.data.end; j++) { // Iterating through customers
        var solj = this.sol[j]
        var cost_soljj = self.cost[solj][j]
        for (var i = 0; i < self.m; i++) { // Iterating though facilities
            var cost_ij = self.cost[i][j]
            var req_ij = self.req[i][j]
            if (cost_ij < cost_soljj && self.cap_left[i] >= req_ij) { // current customerToFacility's cost < another & facility has capacity
                self.cap_left[solj] += self.req[solj][j];
                self.cap_left[i] -= req_ij;
                zz -= cost_soljj;

                self.sol[j] = i;
                zz += cost_ij;

                self.is_improved = true;
                break;
            }
        }
        if (self.is_improved) break;
    }
    self.postMessage({zz: zz});
}