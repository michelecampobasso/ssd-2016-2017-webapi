﻿function readResult(toBePrinted) {
    $("#textArea").val(toBePrinted);
}
function findAll() {
    $.ajax(
        {
            url: "api/My/GetAllCustomers",
            type: "GET",
            contentType: "application/json",
            data: "",
            success: function (result) { readResult(result); },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).message;
                alert(err);
            }
        });
}

function findById() {
    var id = $("#customerId").val();
    $.ajax(
        {
            url: "api/My/GetCustomerById/" + id,
            type: "GET",
            contentType: "application/json",
            data: "",
            success: function (result) { readResult(result); },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).message;
                alert(err);
            }
        });
}

function findAllCustomerOrders() {
    var id = $("#customerId").val();
    $.ajax(
        {
            url: "api/My/GetCustomerOrders/" + id,
            type: "GET",
            contentType: "application/json",
            data: "",
            success: function (result) { readResult(result); },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).message;
                alert(err);
            }
        });
}

function insertCustomer() {
    var id = $("#customerId").val();
    var quant = $('#quantityToUpdate').val();
    var facil = $('#facilityIdToUpdate').val();
    var options = {};
    options.url = "/api/My/InsertCustomer";
    options.type = "POST";
    options.data = JSON.stringify({
        "id": id,
        "req": quant,
        "mag": facil
    });
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (msg) {
        alert(msg);
    };
    options.error = function (err) { alert(err.statusText); };
    $.ajax(options);
}

function updateCustomer() {
    var id = $("#customerId").val();
    var quant = $('#quantityToUpdate').val();
    var facil = $('#facilityIdToUpdate').val();
    var options = {};
    options.url = "/api/My/UpdateCustomer";
    options.type = "PUT";
    options.data = JSON.stringify({
        "id": id,
        "req": quant,
        "mag": facil
    });
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (msg) {
        alert(msg);
    };
    options.error = function (err) { alert(err.statusText); };
    $.ajax(options);
}

function deleteCustomer() {
    var options = {};
    options.url = "/api/My/DeleteCustomer/" +
        $("#customerId").val();
    options.type = "DELETE";
    options.contentType = "application/json";
    options.success = function (msg) {
        alert(msg);
    };
    options.error = function (err) { alert(err.statusText); };
    $.ajax(options);
}

function AjaxHelper(baseUrl) {
    this._baseUrl = baseUrl;
    var callWebAPI = function (url, verb, data, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function (evt) {
            var data = JSON.parse(evt.target.responseText);
            callback(data);
        }
        xhr.onerror = function () {
            alert("Error while calling Web API");
        }

        xhr.open(verb, url);
        xhr.setRequestHeader("Content-Type", "application/json");
        if (data == null) xhr.send();
        else xhr.send(data);
    }
    this.getItem = function (id, callback) {
        callWebAPI(this._baseUrl + "/" + id, "GET", null, callback);
    }
}

function getInstance() {
    var ajaxHelper = new AjaxHelper("/api/My");
    var getGAPCallback = function (res) {
        readResult(res);
    }
    var actionCallback = function (msg) {
        readResult(msg);
    }
    ajaxHelper.getItem("GetGAPInstance", getGAPCallback);
}