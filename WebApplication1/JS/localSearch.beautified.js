﻿function runOpt10() {
    zlbBest = Number.MAX_VALUE
    var z = opt10(c);
    var z_check = checkSol(sol);
    if (z == z_check) $("#textArea").val($("#textArea").val() + "\nSoluzione ammissibile! Costi ottimizzati con ricerca locale 10 (zUB ottimizzato) = " + z);
    else $("#textArea").val($("#textArea").val() + "\nSoluzione non ammissibile.");
}

/*
 *  Belongs to the category of local search euristics.
 *  This algorythm analyzes a previous solution (given for example from a constructive algorythm)
 *  and increasingly improves it, trying to assing any single customer to a specific facility
 *  that owns sufficient capacity left.
 *
 *  This technique's aim is to minimize unused capacity of different facilities. So, it is possible
 *  to reduce costs.
 */

function opt10(cost) {

    var is_improved;
    var z = 0;
    var cap_left = cap.slice();
    var localReq = req;

    for (var j = 0; j < n; j++) {
        var solj = sol[j]
        cap_left[solj] -= localReq[solj][j];
        z += cost[solj][j];
    }
    do {
        is_improved = false;
        for (var j = 0; j < n; j++) { // Iterating through customers
            var solj = sol[j]
            var cost_soljj = cost[solj][j]
            for (var i = 0; i < m; i++) { // Iterating though facilities
                var cost_ij = cost[i][j]
                var req_ij = localReq[i][j]
                if (cost_ij < cost_soljj && cap_left[i] >= req_ij) { // current customerToFacility's cost < another & facility has capacity
                    cap_left[solj] += localReq[solj][j];
                    cap_left[i] -= req_ij;
                    z -= cost_soljj;

                    sol[j] = i;
                    z += cost_ij;

                    is_improved = true;
                    break;
                }
            }
            if (is_improved) break;
        }
    } while (is_improved);
    return z;
}

/*
 *  Belongs to the category of local search euristics.
 *  This uses the structural properties of possible solutions to get fast results.
 *  It generally produces good results for problems like MST but can lead to impossible solutions.
 *
 *  This technique sorts customers in a decreasing order and assigns each of them to the facility
 *  that has the highest possiblity to fulfill the requests.
 */
function constructive() {
    var dist = new Array(m);
    var cap_left = new Array(m);
    sol = new Array(n);
    var zub = 0;

    for (i = 0; i < m; i++) {
        dist[i] = new Array(2);
        cap_left[i] = cap[i]
    }

    for (j = 0; j < n; j++) { // Iterating through customers
        for (i = 0; i < m; i++) { // Iterating through facilities
            dist[i][0] = req[i][j]; 
            dist[i][1] = i;
        }

        dist.sort((function (x, y) { return x[0] - y[0] }));

        for (k = 0; k < m; k++) {
            i = dist[k][1];
            if (cap_left[i] >= req[i][j]) {
                cap_left[i] -= req[i][j]
                sol[j] = i
                zub += c[i][j]
                break;
            }
        }
    }
    if (checkSol(sol) == zub) $("#textArea").val($("#textArea").val() + "\nSoluzione ammissibile! Upper Bound dei costi (zUB) =" + zub)
    else $("#textArea").val($("#textArea").val() + "\nSoluzione non ammissibile!")
    return sol;
}
