﻿//Global parameters
var data = new Object; 
var numLines = 1;
var names = [];
var datablock = [];
var lines;
var cht;

//Partial auto-correlation chart
/*function callPACF() {
    
    d3.json('http://astarte.csr.unibo.it/gapdata/homemade/bogota.json', function (error, json) {
        if (error) return console.warn(error);
        else {
            data.cost = [].concat.apply([], json.cost);
            data.req = [].concat.apply([], json.req);
            calculatePACF();
        }
    });
}

function calculatePACF() {
    var dObj = ssci.ts.pacf()
        .data(data.req)
        .x(function (d) { return d.cost; })
        .y(function (d) { return d.req; })
        .maxlag(15)
        .diff(0);

    dObj();

    var slines = d3.line()
        .x(function (d) { return x_scale(d[0]); })
        .y(function (d) { return y_scale(d[1]); })
        //.curve(function (points) { return ssci.interpString('cubic', points); });

    names = ["cost", "req"];
    var lineNames = ["Lag", "ACF"];
    datablock = data;
    console.log(data);
    //Define charting variables
    var chart_width = 600;
    var chart_height = 400;
    var margin_top = 30;
    var margin_bottom = 35;
    var margin_left = 80;
    var margin_right = 30;
    var chart_title = "Auto-correlation of requests from customers";
    var xaxis_title = "Lag";
    var yaxis_title = "ACF";
    var tt_width = 100;

    //Create SVG object
    cht = d3.select("body")
        .append("svg")
        .attr("class", "c_chart")
        .attr("width", chart_width)
        .attr("height", chart_height);

    //Scales
    var x_scale = d3.scaleLinear()
        .domain([0, 20])
        .range([margin_left, chart_width - margin_right]);
    var y_scale = d3.scaleLinear()
        .domain([-1, 1])
        .range([chart_height - margin_bottom, margin_top]);

    //X Axis
    var xAxis = d3.axisBottom()
        .scale(x_scale);

    cht.append("g")
        .attr("class", "c_x_axis")
        .attr("transform", "translate(0," + (chart_height - margin_bottom) + ")")
        .call(xAxis);

    //Y Axis
    var yAxis = d3.axisLeft()
        .scale(y_scale);

    cht.append("g")
        .attr("class", "c_y_axis")
        .attr("transform", "translate(" + margin_left + ",0)")
        .call(yAxis);

    //Add a title
    cht.append("text")
        .attr("y", 20)
        .attr("x", ((chart_width - margin_left - margin_right) / 2 + margin_left))
        .attr("class", "c_title")
        .text(chart_title);

    //Add axes titles
    cht.append("text")
        .attr("y", chart_height - margin_bottom + 30)
        .attr("x", ((chart_width - margin_left - margin_right) / 2 + margin_left))
        .attr("class", "c_axis_title")
        .text(xaxis_title);

    cht.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", margin_left - 50)
        .attr("x", -((chart_height - margin_top - margin_bottom) / 2 + margin_top))
        .attr("class", "c_axis_title")
        .text(yaxis_title);

    //Lines - acf						
    var points = ssci.objectToPoints(data.req, "req", "cost");
    var dObj = ssci.ts.pacf()
        .data(data.req)
        .x(function (d) { return d.req; })
        .y(function (d) { return d.cost; })
        .diff(0);

    dObj();

    lines = d3.line()
        .x(function (d) { return x_scale(d[0]); })
        .y(function (d) { return y_scale(d[1]); }).curve(d3.curveLinear);

    console.log(dObj.output())
    cht.append("path")
        .data(dObj.output())
        .attr("d", lines(dObj.output()[0]))
        .attr("class", "c_line1");

    //Vertical highlight line
    cht.append("path")
        .attr("class", "c_v_high")
        .attr("d", "M" + margin_left + "," + margin_top + "L" + margin_left + "," + (chart_height - margin_bottom))
        .attr("opacity", "0.0");

    //Grid lines on ticks
    var ticks = y_scale.ticks();
    ticks.shift();

    cht.selectAll(".c_y_grid")
        .data(ticks)
        .enter().append("path")
        .attr("d", function (d, i) { return "M" + margin_left + "," + y_scale(d) + "L" + (chart_width - margin_right) + "," + y_scale(d); })
        .attr("class", "c_y_grid");

    //Tooltips
    //This will consist of at least 3 elements - containing rectangle, x value, y value
    var tt_group = cht.append("g")
        .attr("x", margin_left)
        .attr("y", margin_top)
        .attr("class", "c_tt_group")
        .attr("opacity", "0.0");

    var tt_rect = tt_group.append("rect")
        .attr("class", "c_tt_rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("height", lineNames.length * 10 + 10 + numLines * 5)
        .attr("width", tt_width)
        .attr("opacity", ".5");

    var tt_text = tt_group.selectAll(".c_tt_text")
        .data(lineNames)
        .enter().append("text")
        .attr("class", "c_tt_text")
        .attr("x", "10")
        .attr("y", function (d, i) { return (i * 10 + 15); })
        .text("");

    //var fontSize = parseInt(d3.select(".c_tt_text").style("font-size").match("\\d+")[0]);

    var fontSize = 5;

    //Now need to set y value of tt_text to proper value
    tt_text.attr("y", function (d, i) { return (i * fontSize + (fontSize + 6)); });
    //And height of rectangle
    tt_rect.attr("height", lineNames.length * fontSize + 10 + numLines * 5);

    cht.on("mouseover", function () {
        //Get mouse coordinates
        var xm = d3.mouse(this)[0];
        var ym = d3.mouse(this)[1];

        //Now find nearest point
        var xtemp = dObj.map(function (d) { return Math.abs(x_scale(d.x) - xm); })
        var xmin = xtemp.indexOf(d3.min(xtemp));

        //And move highlight lines
        d3.select(".c_v_high")
            .attr("d", "M" + x_scale(dObj[xmin].x) + "," + margin_top + "L" + x_scale(dObj[xmin].x) + "," + (chart_height - margin_bottom))
            .attr("opacity", "1.0");

        //Move tooltip
        var xpos = x_scale(dObj[xmin].x) + 10;
        if (xpos + tt_width > chart_width) {
            xpos = xpos - tt_width - 20;
        }
        var ypos = ym + 10;
        var ttr_height = Number(tt_rect.attr("height"));
        if (ypos + ttr_height + 10 > chart_height) {
            ypos = chart_height - ttr_height - 10;
        }

        d3.select(".c_tt_group")
            .attr("transform", "translate(" + (xpos) + "," + (ypos) + ")")
            .attr("opacity", "1.0");
        d3.selectAll(".c_tt_text")
            .text(function (d, i) {
                if (i === 0) {
                    return lineNames[i] + " : " + dObj[xmin].x;
                } else {
                    return lineNames[i] + " : " + d3.round(dObj[xmin].y, 2);
                }
            });
    })
        .on("mouseout", function () {
            d3.select(".c_v_high").attr("opacity", "0.0");
            d3.select(".c_tt_group").attr("opacity", "0.0");
        })
        .on("mousemove", function () {
            //Get mouse coordinates
            var xm = d3.mouse(this)[0];
            var ym = d3.mouse(this)[1];

            //Now find nearest point
            var xtemp = dObj.map(function (d) { return Math.abs(x_scale(d.x) - xm); })
            var xmin = xtemp.indexOf(d3.min(xtemp));

            //And move highlight lines
            d3.select(".c_v_high")
                .attr("d", "M" + x_scale(dObj[xmin].x) + "," + margin_top + "L" + x_scale(dObj[xmin].x) + "," + (chart_height - margin_bottom))
                .attr("opacity", "1.0");

            //Move tooltip
            var xpos = x_scale(dObj[xmin].x) + 10;
            if (xpos + tt_width > chart_width) {
                xpos = xpos - tt_width - 20;
            }
            var ypos = ym + 10;
            var ttr_height = Number(tt_rect.attr("height"));
            if (ypos + ttr_height + 10 > chart_height) {
                ypos = chart_height - ttr_height - 10;
            }

            d3.select(".c_tt_group")
                .attr("transform", "translate(" + (xpos) + "," + (ypos) + ")")
                .attr("opacity", "1.0");
            d3.selectAll(".c_tt_text")
                .text(function (d, i) {
                    if (i === 0) {
                        return lineNames[i] + " : " + dObj[xmin].x;
                    } else {
                        return lineNames[i] + " : " + d3.round(dObj[xmin].y, 2);
                    }
                });
        });

    //createGraph();

    cht.append("path")
        .data(dObj.output())
        .attr("d", slines(dObj.output()))
        .attr("class", "c_sline1");

}

function createGraph() {
    
    
}*/