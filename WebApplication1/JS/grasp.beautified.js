﻿function grasp() {
    if (document.getElementById("maxo").checked)
        runGraspChallenge()
    else
        runGraspNoChallenge()
}

function runGraspChallenge() {

    var max_time = $("#maxSeconds").val();
    var candListSize = $("#candListSize").val();
    var z;
    var zub = Number.MAX_VALUE
    var best = sol.slice()

    var endTime = Date.now() + (max_time * 1000)
    while (endTime > Date.now()) {
        z = GraspConstruct(candListSize)
        if (z < zub) {
            zub = z
            best = sol.slice()
        }
        z = localOpt10(c)
        if (z < zub) {
            zub = z
            best = sol.slice()
        }
    }

    if (zub != Number.MAX_VALUE) {
        $("#textArea").val($("#textArea").val() + "\nSoluzione ammissibile! Costi ottimizzati con GRASP (zUB ottimizzato) = " + zub)
        solbest = best
    }
    else $("#textArea").val($("#textArea").val() + "\nSoluzione non ammissibile.")

}

function runGraspNoChallenge() {

    var max_iter = $("#iterations").val();
    var max_time = $("#maxSeconds").val();
    var is_challenge = document.getElementById("maxo").checked;
    var candListSize = 20;
    var iter = 0, z;

    zub = Number.MAX_VALUE

    var endTime = Date.now() + (max_time * 1000)
    if (is_challenge)
        max_iter = Number.MAX_VALUE
    while (iter < max_iter & endTime > Date.now()) {
        z = GraspConstruct(candListSize)
        if (z < zub) {
            zub = z
            solbest = sol.slice()
        }
        z = localOpt10(c)
        if (z < zub) {
            zub = z
            solbest = sol.slice()
        }
        iter++
    }

    if (zub != Number.MAX_VALUE) {
        $("#textArea").val($("#textArea").val() + "\nSoluzione ammissibile! Costi ottimizzati con GRASP (zUB ottimizzato) = " + zub)
        if (iter < max_iter)
            $("#textArea").val($("#textArea").val() + " (time out) - iteration count: " + iter)
        else
            $("#textArea").val($("#textArea").val() + " (max iterations)")
    }
    else $("#textArea").val($("#textArea").val() + "\nSoluzione non ammissibile.")

}


function GraspConstruct(candListSize) {
    var i, j, z = 0;
    var dist = new Array(m);
    var capLeft = new Array(m);
    var localReq = req;
    var localCap = cap;
    var localC = c;
    for (i = 0; i < m; i++) {
        dist[i] = new Array(2);
        capLeft[i] = localCap[i];
    }
    for (j = 0; j < n; j++) { //cliente
        for (i = 0; i < m; i++) { //magazzino
            dist[i][0] = localReq[i][j];
            dist[i][1] = i;
        }
        dist.sort(function (a, b) { return b - a });
        icand = Math.floor(Math.random() * candListSize + 0.5);
        var ii = 0;
        while (ii < m) {
            i = dist[(ii + icand) % m][1];
            if (capLeft[i] >= localReq[i][j]) {
                sol[j] = i;
                capLeft[i] -= localReq[i][j];
                z += localC[i][j];
                break;
            }
            ii++;
        }
    }
    return z;
}


function localOpt10(cost) {
    var is_improved;
    var z = 0;
    var cap_left = cap.slice();
    var localReq = req;

    for (var j = 0; j < n; j++) {
        var solj = sol[j]
        cap_left[solj] -= localReq[solj][j];
        z += cost[solj][j];
    }
    do {
        is_improved = false;
        for (var j = 0; j < n; j++) { // Iterating through customers
            var solj = sol[j]
            var cost_soljj = cost[solj][j]
            for (var i = 0; i < m; i++) { // Iterating though facilities
                var cost_ij = cost[i][j]
                var req_ij = localReq[i][j]
                if (cost_ij < cost_soljj && cap_left[i] >= req_ij) { // current customerToFacility's cost < another & facility has capacity
                    cap_left[solj] += localReq[solj][j];
                    cap_left[i] -= req_ij;
                    z -= cost_soljj;

                    sol[j] = i;
                    z += cost_ij;

                    is_improved = true;
                    break;
                }
            }
            if (is_improved) break;
        }
    } while (is_improved);
    return z;
}