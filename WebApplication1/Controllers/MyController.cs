﻿using System.Web.Http;
using System.Configuration;
using Newtonsoft.Json;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    public class MyController : ApiController
    {

        string connString, queryText, factory;
        MyModel model = null;

        private void preProtocol(string connStr, string query, string factory)
        {
            if (model == null)
            {
                model = new MyModel();
            }
            connString = connStr;
            queryText = query;
            this.factory = factory;
        }

        [HttpGet]                       // in esecuzione solo con un get dal client
        [ActionName("GetAllCustomers")]    // nome del metodo esposto nella API
        public string GetAllCustomers()
        {
            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, 
                "SELECT id, req, mag FROM clienti", "System.Data.SQLite");
            return model.readTableViaF(connString, queryText, factory);
        }

        [HttpGet]
        [ActionName("GetCustomerById")]    // nome del metodo esposto nella API
        public string GetCustomerById(int id)
        {
            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, 
                "SELECT id, req, mag FROM clienti WHERE id=" + id, "System.Data.SQLite");
            return model.readTableViaF(connString, queryText, factory);
        }

        [HttpGet]     // in esecuzione solo con un get dal client 
        [ActionName("GetCustomerOrders")]   // nome del metodo esposto 
        public string GetCustomerOrders(int id)
        {
            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, 
                "select id, req from clienti where id=" + id, "System.Data.SQLite");
            return model.readTableViaF(connString, queryText, factory);

            /*Clienti c = JsonConvert.DeserializeObject<List<Clienti>>(s).FirstOrDefault((u) => u.id == id);
            IHttpActionResult asd;
            return c == null ? asd = NotFound() : asd = Ok(c);*/
        }

        [HttpPost]
        [ActionName("InsertCustomer")]
        public string InsertCustomer(Clienti obj)
        {
            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, 
                "insert into clienti (id, req, mag) values("  + obj.id + "," + obj.req + ",'" + obj.mag + "')", "System.Data.SQLite");
            model.execNonQueryViaF(connString, queryText, factory);
            return "Customer inserted";
        }

        [HttpPut]
        [ActionName("UpdateCustomer")]
        public string UpdateCustomer(Clienti obj)
        {
            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString,
                "update clienti set req = " + obj.req + ", mag = " + obj.mag + " WHERE id = " + obj.id, "System.Data.SQLite");
            model.execNonQueryViaF(connString, queryText, factory);
            return "Customer updated";
        }

        [HttpDelete]
        [ActionName("DeleteCustomer")]
        public string DeleteCustomer(int id)
        {
            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, "delete from clienti where id=" + id, "System.Data.SQLite");
            model.execNonQueryViaF(connString, queryText, factory);
            return "Customer deleted";
        }

        public string GetGAPInstance() //localhost:65355/api/my/getgapinstance?id=1 -> "non trovato"
        {
            string res;
            int numCustomer, numFacilities;

            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, "select id, mag, req from clienti", "System.Data.SQLite");
            res = model.readTableViaF(connString, queryText, factory);
            Cli[] clients = JsonConvert.DeserializeObject<Cli[]>(res);

            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, "select id, cap from magazzini", "System.Data.SQLite");
            res = model.readTableViaF(connString, queryText, factory);
            Mag[] mags = JsonConvert.DeserializeObject<Mag[]>(res);

            numFacilities = mags.Length;
            numCustomer = clients.Length / numFacilities;

            preProtocol(ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString, "select id, mag, cli, cost from costi", "System.Data.SQLite");
            res = model.readTableViaF(connString, queryText, factory);
            Cost[] costs = JsonConvert.DeserializeObject<Cost[]>(res);

            int[,] cost = new int[numFacilities, numCustomer];
            for (int i = 0; i < costs.Length; i++)
            {
                cost[costs[i].mag - 1, costs[i].cli - 1] = costs[i].cost;
            }

            int[,] req = new int[numFacilities, numCustomer];
            for (int j = 0; j < clients.Length; j++)
            {
                req[clients[j].mag - 1, clients[j].id - 1] = clients[j].req;
            }

            int[] cap = new int[numFacilities];
            for (int i = 0; i < mags.Length; i++)
            {
                cap[mags[i].id - 1] = mags[i].cap;
            }

            Instance inst = new Instance()
            {
                name = "fromDB",
                numcustomers = numCustomer,
                numfacilities = numFacilities,
                cost = cost,
                req = req,
                cap = cap
            };

            return JsonConvert.SerializeObject(inst);
        }

        public class Cli
        {
            public int id;
            public int mag;
            public int req;
        }

        public class Mag
        {
            public int id;
            public int cap;
        }


        public class Cost
        {
            public int id;
            public int mag;
            public int cli;
            public int cost;
        }

        public class Instance
        {
            public string name;
            public int numcustomers;
            public int numfacilities;
            public int[,] cost, req;
            public int[] cap;
        }
    }
}