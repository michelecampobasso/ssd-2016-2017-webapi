﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class MyModel
    {
        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento (mittente e testo generato per l'interfaccia)

        public event viewEventHandler FlushText;  // questo genera l'evento

        private IDbConnection CreateConnectionThroughExplicitRequest(ConnectionStringSettings connectionStringSettings, bool isSqlLiteDb)
        {
            IDbConnection connection = null;
            if (isSqlLiteDb)
                connection = new SQLiteConnection(connectionStringSettings.ConnectionString);
            else
                connection = new SqlConnection(connectionStringSettings.ConnectionString);
            connection.Open();

            return connection;
        }

        private IDbConnection CreateConnectionThroughDBFactory(string dbFactoryName, ConnectionStringSettings connectionStringSettings)
        {
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(dbFactoryName);
            IDbConnection connection = dbFactory.CreateConnection();
            connection.ConnectionString = connectionStringSettings.ConnectionString;
            connection.Open();

            return connection;
        }

        private IDataReader QueryDataFromDB(IDbConnection connection, string query, Dictionary<string, object> paramsToBeQueried)
        {
            IDbCommand command = connection.CreateCommand();
            command.CommandText = query;

            // Adding every needed parameter in a safe way
            foreach (KeyValuePair<string, object> param in paramsToBeQueried)
            {
                IDbDataParameter safeParam = command.CreateParameter();
                // safeParam.DbType = DbType.Int32; <<<----- Removed thanks to the automatic inference of types and for the check I do in the View.
                safeParam.ParameterName = "@" + param.Key;
                safeParam.Value = param.Value;
                command.Parameters.Add(safeParam);
            }

            return command.ExecuteReader();
        }

        private void PrintDataFromReader(IDataReader reader, string[] paramsToPrint)
        {
            while (reader.Read())
            {
                // Flushing every single parameter required
                foreach (string par in paramsToPrint)
                    FlushText(this, reader[par] + "\t");
                FlushText(this, Environment.NewLine);
            }
            reader.Close();
        }

        public void ConnectAndRead(ConnectionStringSettings connectionStringSettings, bool isSqlLiteDb, string idSerie)
        {
            IDbConnection connection = null;
            try
            {
                connection = CreateConnectionThroughExplicitRequest(connectionStringSettings, isSqlLiteDb);

                var paramsToBeShown = new string[] { "periodo", "val" };
                var paramsToBeQueried = new Dictionary<string, object>();
                paramsToBeQueried["idserie"] = idSerie;

                IDataReader reader = QueryDataFromDB(connection, "select h.periodo, h.val from histordini as h WHERE idserie=" + idSerie, paramsToBeQueried);
                PrintDataFromReader(reader, paramsToBeShown);

            }
            catch (Exception e)
            {
                FlushText(this, "[FillDataSet] Error: " + e.Message + Environment.NewLine);
            }
            finally
            {
                if (connection != null)
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
            }
        }

        public void ConnectWithDataSetAndProviderFactoryAndRead(ConnectionStringSettings connectionStringSettings, string dbFactoryName, string idSerie)
        {
            IDbConnection connection = null;
            try
            {
                connection = CreateConnectionThroughDBFactory(dbFactoryName, connectionStringSettings);

                var paramsToBeShown = new string[] { "periodo", "val" };
                var paramsToBeQueried = new Dictionary<string, object>();
                paramsToBeQueried["idserie"] = idSerie;

                IDataReader reader = QueryDataFromDB(connection, "select h.periodo, h.val from histordini as h WHERE idserie=" + idSerie, paramsToBeQueried);
                PrintDataFromReader(reader, paramsToBeShown);
            }
            catch (Exception ex)
            {
                FlushText(this, "[FillDataSet] Error: " + ex.Message + Environment.NewLine);
            }
            finally
            {
                if (connection != null)
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
            }
        }

        public string readTableViaF(string connString, string queryText, string factory)
        {
            int i, numcol;
            string res = "[";
            List<string> columns = new List<string>();
            DbProviderFactory dbFactory = null;

            dbFactory = DbProviderFactories.GetFactory(factory);

            using (DbConnection conn = dbFactory.CreateConnection())
            {
                try
                {
                    conn.ConnectionString = connString;
                    conn.Open();
                    IDbCommand com = conn.CreateCommand();
                    com.CommandText = queryText;
                    IDataReader reader = com.ExecuteReader();

                    numcol = reader.FieldCount;
                    for (i = 0; i < numcol; i++)
                        columns.Add(reader.GetName(i));

                    while (reader.Read())
                    {
                        res += "{";
                        for (i = 0; i < numcol; i++)
                        {
                            res += "\"" + columns[i] + "\":\"" + reader[i] + "\",";
                        }
                        res += "},";
                        res = res.Replace(",}", "}");
                        res += "\n";
                    }
                    reader.Close();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    FlushText(this, ex.Message);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
            return (res + "]").Replace(",]", "]");
        }

        public int execNonQueryViaF(string connString, string queryText, string factory)
        {
            int numRows = 0;
            DbProviderFactory dbFactory = null;

            dbFactory = DbProviderFactories.GetFactory(factory);

            using (DbConnection conn = dbFactory.CreateConnection())
            {
                try
                {
                    conn.ConnectionString = connString;
                    conn.Open();
                    IDbCommand com = conn.CreateCommand();
                    com.CommandText = queryText;
                    numRows = com.ExecuteNonQuery();

                    conn.Close();
                }
                catch (Exception ex)
                {
                    FlushText(this, ex.Message);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
            return numRows;
        }


        /* List<histordini> selectedOrders = new List<histordini>();
        foreach (histordini o in selectedHistory)
        {
            selectedOrders.Add(o);
        }*/

        // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv 
        // FlushText(this, JsonConvert.SerializeObject(myOrdini)); <<<<------- Serializzatore JSon per WebAPI <<<<<<<<<<<<<<<<<<<<<<<
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    }
}