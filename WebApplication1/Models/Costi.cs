﻿namespace WebApplication1.Models
{
    public class Costi
    {
        public int id { get; set; }
        public int mag { get; set; }
        public int cli { get; set; }
        public double cost { get; set; }
    }
}